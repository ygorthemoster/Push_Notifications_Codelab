# Google Codelab: Add Your Web App to a User's Home Screen Codelab
This is an implementation of a Google codelab tutorial, the tutorial is freely avaliable at: https://codelabs.developers.google.com/codelabs/push-notifications/index.html, this is a simple tutorial that demonstrates the basics of service workers and hoe to handle push notifications via them, this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)
- A webserver as per tutorial I'll be using [web-server for chrome](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb)

## Installing

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Cloud_Functions_Codelab.git
```

Get Application Server Keys
To work try out this you need to generate some application server keys which we can do with this [site](https://web-push-codelab.appspot.com/)

after this copy and paste your public key in the main.js file in the line that reads:

```javascript
const applicationServerPublicKey = //KEY HERE
```

then start the app in web server, don't forget to auto-serve the index.html file


## Running
You can view the app by accessing the hosting link provided by the webserver, if you are using web-server for chrome normally it'll be at: http://127.0.0.1:8887

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source by [GoogleChrome](https://github.com/GoogleChrome) available at: https://github.com/GoogleChrome/push-notifications.git
- Tutorial by [Google Codelabs](https://codelabs.developers.google.com/) available at: https://codelabs.developers.google.com/codelabs/push-notifications/index.html
